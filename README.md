# scRNAseq.analysis

A container for advanced single-cell RNA-seq analysis using RStudio.

### Building the container

```
docker build --squash -t scrnaseq.analysis:r3.6.2_bioc3.10 https://gitlab.gwdg.de/loosolab/container/scrnaseq.analysis.git
```

### Running the container

```
docker run -d -p 172.16.12.92:8787:8787 -e USERID=$UID -v /path/to/workspace:/home/rstudio/path/to/workspace scrnaseq.analysis:r3.6.2_bioc3.10
```
